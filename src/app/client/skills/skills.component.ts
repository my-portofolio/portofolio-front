import { Component, OnInit } from '@angular/core';
import {ClientDataService} from '../client-data.service';
import {Skills} from '../../shared/Models/Skills';
import {animate, keyframes, query, stagger, style, transition, trigger} from '@angular/animations';
import {Experience} from '../../shared/Models/Experience';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss'],
    animations: [
        trigger('listAnimation', [
            transition('* => *', [
                query(':enter', style({opacity: 0 }), {optional: true }),
                query(':enter', stagger('200ms', [
                    animate('1s ease-in', keyframes([
                        style({opacity: 0, transform: 'translateY(-100px)', offset: 0}),
                        style({opacity: 0.5, transform: 'translateY(70px)', offset: 0.3}),
                        style({opacity: 1, transform: 'translateY(0)', offset: 1}),
                    ]))
                ]), {optional: true }),
                query(':leave', style({opacity: 0 }), {optional: true }),
                query(':leave', stagger('300ms', [
                    animate('1s ease-in', keyframes([
                        style({opacity: 1, transform: 'translateY(0)', offset: 0}),
                        style({opacity: 0.5, transform: 'translateY(35px)', offset: 0.3}),
                        style({opacity: 0, transform: 'translateY(-75px)', offset: 1}),
                    ]))
                ]), {optional: true }),

            ])
        ]),
    ]
})
export class SkillsComponent implements OnInit {
  private all_skills: Skills[] =  [];
  private all_experience: Experience[] =  [];

  constructor(private skills_service: ClientDataService) { }

  ngOnInit() {
     this.skills_service.getSkillsData().subscribe(data => this.all_skills = data);
     this.skills_service.getExperiencesData().subscribe(data => this.all_experience = data);
  }

}
