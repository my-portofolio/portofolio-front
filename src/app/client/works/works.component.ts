import { Component, OnInit } from '@angular/core';
import {ClientDataService} from '../client-data.service';
import {Projects} from '../../shared/Models/Projects';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-works',
  templateUrl: './works.component.html',
  styleUrls: ['./works.component.scss']
})
export class WorksComponent implements OnInit {
    private all_projects: Projects[] =  [];
    constructor(private projects_service: ClientDataService, private _sanitizer: DomSanitizer) { }
    image_directory = 'http://127.0.0.1:8000/storage/';

    ngOnInit() {
        this.projects_service.getProjectsData().subscribe(data => this.all_projects = data);
    }
    getimage(image_path: string) {
        return this._sanitizer.bypassSecurityTrustStyle('url(' + image_path + ')');
    }

}
