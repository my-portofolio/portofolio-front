import { Component, OnInit } from '@angular/core';
import {Website} from '../../shared/Models/Website';
import {ClientDataService} from '../client-data.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

    footer_config: Website = new Website();

    constructor(private dataservice: ClientDataService) { }

    ngOnInit() {
        this.dataservice.get_website_data()
            .subscribe(data => this.footer_config = data);

    }

}
