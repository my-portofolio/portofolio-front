import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AllRoutingComponent, ClientRoutingModule} from './client-routing.module';
import {NavbarModule} from 'angular-bootstrap-md';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ClientDataService} from './client-data.service';
import {PortoInterceptorService} from './_services/porto-interceptor.service';

@NgModule({
  declarations: [
      AllRoutingComponent,
  ],
  imports: [
      CommonModule,
      ClientRoutingModule,
      NavbarModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,

  ],
    providers: [
        ClientDataService,
        {provide: HTTP_INTERCEPTORS,
            useClass: PortoInterceptorService,
            multi: true }
    ],

})
export class ClientModule { }
