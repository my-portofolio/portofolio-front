import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})

export class ContactComponent implements OnInit {
    subject = '';
    formGroup: FormGroup;
    post: any;
    message = '';
    name = '';
    email = '';
    alert_requier = 'Please Fill this fields';
    alert_email = 'Please enter a valid email';
  constructor(private formBuilder: FormBuilder) {
    this.formGroup = this.formBuilder.group({
        'name': [null, Validators.required],
        'email': [null, Validators.compose([Validators.required, Validators.email])],
        'subject' : [null, Validators.compose([Validators.required])],
        'message': [null, Validators.compose([Validators.required, Validators.minLength(15)])],
    });
  }
  ngOnInit() {
  }

  SendMessage(post) {
      this.name = post.name;
      this.email = post.email;
      this.message = post.message;
      this.subject = post.subject;
  }

}
