import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Website} from '../shared/Models/Website';
import {Domaines} from '../shared/Models/Domaines';
import {Experience} from '../shared/Models/Experience';
import {Projects} from '../shared/Models/Projects';
import {Skills} from '../shared/Models/Skills';
import {ISociales} from '../shared/Models/ISociales';
import {User} from '../shared/Models/User';
import {isNull} from 'util';

@Injectable({
  providedIn: 'root'
})
export class ClientDataService {

    private url_client = 'http://127.0.0.1:8000/api/';
    constructor(private _request: HttpClient) { }

    public get_website_data(): Observable <Website> {
        return this._request.get<Website>(this.url_client.concat('website'));
    }
    public getDomaineData(): Observable <Domaines[]> {
        return this._request.get<Domaines[]>(this.url_client.concat('domaines'));
    }
    public getExperiencesData(): Observable <Experience[]> {
        return this._request.get<Experience[]>(this.url_client.concat('experiences'));
    }
    public getProjectsData(): Observable <Projects[]> {
        return this._request.get<Projects[]>(this.url_client.concat('projects'));
    }
    public getSkillsData(): Observable <Skills[]> {
        return this._request.get<Skills[]>(this.url_client.concat('skills'));
    }
    public getSocialsData(): Observable <ISociales[]> {
        return this._request.get<ISociales[]>(this.url_client.concat('sociales'));
    }
    public getUserData(): Observable <User> {
        return this._request.get<User>(this.url_client.concat('user'));
    }
    // public verify_web_token() {
    //     let web_token: string;
    //     return this._request.get<any>('web_config.json')
    //         .subscribe(data => web_token = data['web_token']);
    //     if (isNull(web_token)) {return false;
    //     } else {return true; }
    // }
}
