import { Component, OnInit } from '@angular/core';
import {Domaines} from '../../shared/Models/Domaines';
import {ClientDataService} from '../client-data.service';
import {animate, keyframes, query, stagger, style, transition, trigger} from '@angular/animations';
import {User} from '../../shared/Models/User';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
    animations: [
        trigger('listAnimation', [
            transition('* => *', [
                query(':enter', style({opacity: 0 }), {optional: true }),
                query(':enter', stagger('300ms', [
                    animate('1s ease-in', keyframes([
                        style({opacity: 0, transform: 'translateX(-75px)', offset: 0}),
                        style({opacity: 0.5, transform: 'translateX(30px)', offset: 0.3}),
                        style({opacity: 1, transform: 'translateX(0)', offset: 1}),
                    ]))
                ]), {optional: true }),
                query(':leave', style({opacity: 0 }), {optional: true }),
                query(':leave', stagger('300ms', [
                    animate('1s ease-in', keyframes([
                        style({opacity: 1, transform: 'translateY(0)', offset: 0}),
                        style({opacity: 0.5, transform: 'translateY(35px)', offset: 0.3}),
                        style({opacity: 0, transform: 'translateY(-75px)', offset: 1}),
                    ]))
                ]), {optional: true }),

            ])
        ]),
    ]
})
export class HomeComponent implements OnInit {

    all_domaine: Domaines[] = [] ;
    user_desc: User;

  constructor(private data_service: ClientDataService) { }

  ngOnInit() {
      this.data_service.getDomaineData()
          .subscribe(data => this.all_domaine = data);
      this.data_service.getUserData()
          .subscribe(data => this.user_desc = data);
  }

}
