import { Component, OnInit } from '@angular/core';
import {ClientDataService} from '../client-data.service';

import {Website} from '../../shared/Models/Website';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  navbar_config: Website = new Website();

  constructor(private dataservice: ClientDataService) { }

  ngOnInit() {
    this.dataservice.get_website_data()
        .subscribe(data => this.navbar_config = data);

  }

}
