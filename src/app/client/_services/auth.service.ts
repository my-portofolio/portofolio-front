import { Injectable } from '@angular/core';
import {isNull} from 'util';
import {User} from '../../shared/Models/User';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  getWebKey (): string {
    return 'Y2VjaSBlc3QgdW5lIGFwcGxpY2F0aW9uIGRlIHBvcnRvZm9saW8=';
  }

    getUserKey(): string {
       const userToken = <User> JSON.parse(localStorage.getItem('currentUser'));
       return userToken.remember_token;
    }
    isAuthentificated() {
        if (localStorage.getItem('currentUser')) {
          return true;
        } else {
            return false;
        }
    }
}
