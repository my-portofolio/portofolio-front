import { TestBed } from '@angular/core/testing';

import { PortoInterceptorService } from './porto-interceptor.service';

describe('PortoInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PortoInterceptorService = TestBed.get(PortoInterceptorService);
    expect(service).toBeTruthy();
  });
});
