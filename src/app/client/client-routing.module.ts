import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {SkillsComponent} from './skills/skills.component';
import {WorksComponent} from './works/works.component';
import {ContactComponent} from './contact/contact.component';

const routes: Routes = [
    {path: '', redirectTo: 'home'},
    {path: 'home', component: HomeComponent, data: { page: 'main'}},
    {path: 'skills', component: SkillsComponent, data: { page: 'skills'}},
    {path: 'works', component: WorksComponent, data: { page: 'work'}},
    {path: 'contact', component: ContactComponent, data: { page: 'contact'}},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class ClientRoutingModule { }
export const AllRoutingComponent = [HomeComponent, SkillsComponent, WorksComponent, ContactComponent];

