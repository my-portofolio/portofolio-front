import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminDashboardComponent} from './admin-dashboard/admin-dashboard.component';
import {LoginComponent} from './login/login.component';
import {DomaineManageComponent} from './domaine-manage/domaine-manage.component';
import {SkillsManageComponent} from './skills-manage/skills-manage.component';
import {WorksManageComponent} from './works-manage/works-manage.component';
import {LogoutComponent} from './logout/logout.component';
import {Page404AdminComponent} from './page404-admin/page404-admin.component';
import {PreferencesComponent} from './preferences/preferences.component';

const routes: Routes = [
    {path: '', component: AdminDashboardComponent},
    {path: 'dashboard', component: AdminDashboardComponent},
    {path: 'domaine', component: DomaineManageComponent},
    {path: 'skills', component: SkillsManageComponent},
    {path: 'works', component: WorksManageComponent},
    {path: 'preferences', component: PreferencesComponent},
    {path: 'logout', component: LogoutComponent},
    {path: '**', component: Page404AdminComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
export const AdminRouterComponets = [
    AdminDashboardComponent,
    AdminDashboardComponent,
    DomaineManageComponent,
    SkillsManageComponent,
    WorksManageComponent,
    WorksManageComponent,
    LogoutComponent,
    Page404AdminComponent,
     PreferencesComponent
];
