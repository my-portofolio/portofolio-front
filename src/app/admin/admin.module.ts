import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {AdminRouterComponets, AdminRoutingModule} from './admin-routing.module';
import {AuthService} from '../client/_services/auth.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {PortoInterceptorService} from '../client/_services/porto-interceptor.service';
import {AdminIntercerptorService} from './_services/admin-intercerptor.service';
import {AdminDataAllService} from './_services/admin-data-all.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [AdminRouterComponets],
  imports: [
    CommonModule,
    AdminRoutingModule,
      HttpClientModule,
      ReactiveFormsModule,
      FormsModule
  ],
    providers: [
        AuthService,
        AdminDataAllService,
        {provide: HTTP_INTERCEPTORS,
            useClass: AdminIntercerptorService,
            multi: true }
    ]
})
export class AdminModule { }
