import { TestBed } from '@angular/core/testing';

import { AdminDataAllService } from './admin-data-all.service';
import {Observable} from 'rxjs';
import {User} from '../../shared/Models/User';
import Expected = jasmine.Expected;

describe('AdminDataAllService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminDataAllService = TestBed.get(AdminDataAllService);
    expect(service).toBeTruthy();
  });

});
