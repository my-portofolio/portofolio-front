import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Domaines} from '../../shared/Models/Domaines';
import {Skills} from '../../shared/Models/Skills';
import {Experience} from '../../shared/Models/Experience';
import {Projects} from '../../shared/Models/Projects';
import {Observable} from 'rxjs';
import {User} from '../../shared/Models/User';

@Injectable({
  providedIn: 'root'
})
export class AdminDataAllService {
    private url_client = 'http://127.0.0.1:8000/api/';
    private url_admin = 'http://127.0.0.1:8000/api/portofolio_admin/';
  constructor(private httpCall: HttpClient) { }

    addingDomaines(newDomaine: Domaines) {
        return this.httpCall.get<any>(this.url_admin.concat('domaines'), {params: {'domaine': JSON.stringify(newDomaine)}});
    }
    listDomaine() {
        return this.httpCall.get<Domaines[]>(this.url_client.concat('domaines'));
    }
    listSkills() {
        return this.httpCall.get<Skills[]>(this.url_client.concat('skills'));
    }
    listExperiences() {
        return this.httpCall.get<Experience[]>(this.url_client.concat('experiences'));
    }
    addingSkills(newSkill: Skills) {
        return this.httpCall.get<any>(this.url_admin.concat('skills'), {params: {'skills': JSON.stringify(newSkill)}});
    }
    addingExperience(newExperience: Experience) {
        return this.httpCall.get<any>(this.url_admin.concat('experiences'), {params: {'experiences': JSON.stringify(newExperience)}});
    }

    listProjects() {
        return this.httpCall.get<Projects[]>(this.url_client.concat('projects'));
    }

    addingProject(formdata: FormData, headers_give) {
        return this.httpCall.post<any>(this.url_admin.concat('projects'), formdata, {headers: headers_give});
    }

    deleteDomaine(domaineNumber: number) {
        return this.httpCall.delete<any>(this.url_admin.concat('domaines'), {params: {'domaine_id': JSON.stringify(domaineNumber)}});
    }
    public getUserData(): Observable <User> {
        return this.httpCall.get<User>(this.url_client.concat('user'));
    }

    changeDescription(new_description: string) {
        return this.httpCall.post<any>(this.url_admin.concat('user_desc'), {description: new_description});
    }
}

