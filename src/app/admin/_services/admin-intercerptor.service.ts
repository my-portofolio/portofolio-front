import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from '../../client/_services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminIntercerptorService implements HttpInterceptor {

  constructor(private auth_service: AuthService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        req = req.clone({
            setParams: {
                Authorization: this.auth_service.getWebKey(),
                userToken: this.auth_service.getUserKey()
            }
        });
        return next.handle(req);
    }
}
