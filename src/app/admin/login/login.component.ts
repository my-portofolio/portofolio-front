import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../shared/login.service';
import {User} from '../../shared/Models/User';
import {first, map} from 'rxjs/operators';
import {ActivatedRoute, Route, Router} from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    username = '';
    formGroup: FormGroup;
    post: any;
    password = '';
    alert_password = 'password is required and minimum length is 6';
    alert_username = 'username c\'ant be empty';
    user: User;
    returnUrl: string;

  constructor(private formBuilder: FormBuilder,
              private login_data: LoginService,
              private router: Router,
              private route: ActivatedRoute) {
      this.formGroup = this.formBuilder.group({
          'username': [null, Validators.required],
          'password': [null, Validators.compose([Validators.required, Validators.minLength(6)])]
      });
  }

  ngOnInit() {
      this.login_data.logout();

      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

  }
    SendMessage(post) {
        this.username = post.username;
        this.password = post.password;
        this.login_data.login_verify(this.username, this.password)
            .pipe(first())
            .subscribe(data => {
                if (data.remember_token) {
                    this.router.navigate(['/admin_porto']);
                } else {
                    console.error('undefined');
                }
            },
                error => {
                console.error(error);
            });

    }

}
