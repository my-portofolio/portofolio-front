import { Component, OnInit } from '@angular/core';
import {Skills} from '../../shared/Models/Skills';
import {Experience} from '../../shared/Models/Experience';
import {AdminDataAllService} from '../_services/admin-data-all.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-skills-manage',
  templateUrl: './skills-manage.component.html',
  styleUrls: ['./skills-manage.component.scss']
})
export class SkillsManageComponent implements OnInit {

  all_skills: Skills[] = [];
  all_experiences: Experience[] = [];
    subject = '';
    formGroupSkills: FormGroup;
    formGroupExperience: FormGroup;
    post: any;
    titleSkills = '';
    colorskills = '';
    pourcskills = '';
    alert_requier = 'Please Fill this fields';
    alert_email = 'Please enter a valid email';
    private adding_message: string;
    private adding_message_exp: string;
    private adding_message_skills: string;

  constructor(private _httpservice: AdminDataAllService, private formBuilder: FormBuilder) {
      this.formGroupSkills = this.formBuilder.group({
          'titleskills': [null, Validators.required],
          'colorskills': [null, Validators.required],
          'pourcskills' : [null, Validators.required],
      });
      this.formGroupExperience = this.formBuilder.group({
          'title': [null, Validators.required],
          'desc': [null, Validators.required],
          'date' : [null, Validators.required],
      });
  }

  ngOnInit() {
    this._httpservice.listSkills().subscribe(data => this.all_skills = data);
    this._httpservice.listExperiences().subscribe(data => this.all_experiences = data);
  }

    addingExperience(exp_value) {
        const experience = new Experience(
            exp_value.title,
            exp_value.date,
            exp_value.desc
        );
        this._httpservice.addingExperience(experience).subscribe(
            data => {
                if (data.response === 'success') {
                    this.adding_message_exp = 'This experience was added successfully';
                    this.listOfExperience();
                    this.formGroupExperience.reset();
                } else {
                    this.adding_message = 'Error when adding this domaine';
                }
            }
        );
  }
    addingSkills(skills_value) {
        const skills = new Skills(
            skills_value.colorskills,
            skills_value.titleskills,
            skills_value.pourcskills
        );
        this._httpservice.addingSkills(skills).subscribe(
            data => {
                if (data.response === 'success') {
                    this.adding_message_skills = 'This skill was added successfully';
                    this.listOfSkills();
                    this.formGroupSkills.reset();
                } else {
                    this.adding_message = 'Error when adding this domaine';
                }
            }
        );
  }

    private listOfSkills() {
        this._httpservice.listSkills().subscribe(data => this.all_skills = data);
    }

    private listOfExperience() {
        this._httpservice.listExperiences().subscribe(data => this.all_experiences = data);
    }
}
