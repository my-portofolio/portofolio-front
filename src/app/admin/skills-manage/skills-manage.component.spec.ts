import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillsManageComponent } from './skills-manage.component';

describe('SkillsManageComponent', () => {
  let component: SkillsManageComponent;
  let fixture: ComponentFixture<SkillsManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillsManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillsManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
