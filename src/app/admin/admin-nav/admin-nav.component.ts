import { Component, OnInit } from '@angular/core';
import {User} from '../../shared/Models/User';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-nav',
  templateUrl: './admin-nav.component.html',
  styleUrls: ['./admin-nav.component.scss']
})
export class AdminNavComponent implements OnInit {

  currentUser: User;
  constructor(private router: Router) { }

  ngOnInit() {
     this.currentUser = <User> JSON.parse(localStorage.getItem('currentUser'));
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['/login']);
  }

}
