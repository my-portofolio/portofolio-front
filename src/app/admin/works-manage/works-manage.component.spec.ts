import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorksManageComponent } from './works-manage.component';

describe('WorksManageComponent', () => {
  let component: WorksManageComponent;
  let fixture: ComponentFixture<WorksManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorksManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorksManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
