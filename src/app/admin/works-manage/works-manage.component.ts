import { Component, OnInit } from '@angular/core';
import {AdminDataAllService} from '../_services/admin-data-all.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Projects} from '../../shared/Models/Projects';
import {HttpHeaders} from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-works-manage',
  templateUrl: './works-manage.component.html',
  styleUrls: ['./works-manage.component.scss']
})
export class WorksManageComponent implements OnInit {
    all_projects: Projects[] = [];
    project_interact = {categorie: '', title: '', description: '',
        color: '', image: '', link: '', btncolor: '', file: File};
    image_directory = 'http://127.0.0.1:8000/storage/';

    private adding_message: string;
    formGroupProjects: FormGroup;

    constructor(private _httpservice: AdminDataAllService, private formBuilder: FormBuilder, private _sanitizer: DomSanitizer) {
        this.formGroupProjects = this.formBuilder.group({
            'categorie': [null, Validators.required],
            'title': [null, Validators.required],
            'description': [null, Validators.compose([Validators.required, Validators.minLength(10)])],
            'link': [null, Validators.required],
            'color': [null, Validators.required],
            'btncolor': [null, Validators.required],
            'projectimage': [null, Validators.required],
        });
    }

    ngOnInit() {
        this._httpservice.listProjects().subscribe(data => this.all_projects = data);
    }

    addingProjects() {
        const form_data: FormData = new FormData();
        const headers = new HttpHeaders();
        headers.append('Content-Type', 'multipart/form-data');
        form_data.append('project_title', this.project_interact.title);
        form_data.append('project_categorie', this.project_interact.categorie);
        form_data.append('project_color', this.project_interact.color);
        form_data.append('project_btncolor', this.project_interact.btncolor);
        form_data.append('project_description', this.project_interact.description);
        form_data.append('project_link', this.project_interact.link);
       form_data.append('project_image', this.project_interact.file, this.project_interact.file.name);
        this._httpservice.addingProject(form_data, headers).subscribe(
            data => {
                console.log(data);
                this.adding_message = data;
                if (data.response === 'success') {
                    this.adding_message = data.message;
                    this.listOfProjects();
                    this.formGroupProjects.reset();
                } else {
                    this.adding_message = data.message;
                }
            }
        );
    }
    getimage(image_path: File) {
        return this._sanitizer.bypassSecurityTrustStyle('url(' + image_path + ')');
    }

    private listOfProjects() {
        this._httpservice.listProjects().subscribe(data => this.all_projects = data);
    }

    onCategorie(event) {
        this.project_interact.categorie = event.target.value;
    }
    onTitle(event) {
        this.project_interact.title = event.target.value;
    }
    onColor(event) {
        this.project_interact.color = event.target.value;
    }
    onLink(event) {
        this.project_interact.link = event.target.value;
    }
    onbtnColor(event) {
        this.project_interact.btncolor = event.target.value;
    }
    onDescription(event) {
        this.project_interact.description = event.target.value;
    }

    onFile(event) {
        const fileList: FileList = event.target.files;
        let file: File ;
        if (fileList.length > 0) {
            file = fileList[0];
        } else {
            file = event.target.file;
        }
        this.project_interact.file = file;
    }

    getimagehost(image_serve: string) {
        return this._sanitizer.bypassSecurityTrustStyle('url(' + image_serve + ')');
    }
}
