import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomaineManageComponent } from './domaine-manage.component';

describe('DomaineManageComponent', () => {
  let component: DomaineManageComponent;
  let fixture: ComponentFixture<DomaineManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomaineManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomaineManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
