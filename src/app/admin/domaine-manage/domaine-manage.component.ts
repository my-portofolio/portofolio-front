import { Component, OnInit } from '@angular/core';
import {AdminDataAllService} from '../_services/admin-data-all.service';
import {Domaines} from '../../shared/Models/Domaines';

@Component({
  selector: 'app-domaine-manage',
  templateUrl: './domaine-manage.component.html',
  styleUrls: ['./domaine-manage.component.scss']
})
export class DomaineManageComponent implements OnInit {
    all_domaine: Domaines[] = [];
    adding_message = '';
    domaine_interact = {icon: '', title: '', description: '', color: '', textcolor: ''};
    private delete_message: string;

  constructor(private domaineService: AdminDataAllService) { }

  ngOnInit() {
      this.listOfDomaines();
  }
  adding_domaine() {
      const domaine =
          new Domaines(
              this.domaine_interact.title,
              this.domaine_interact.icon,
              this.domaine_interact.description,
              this.domaine_interact.color,
              this.domaine_interact.textcolor
          );
      this.domaineService.addingDomaines(domaine)
          .subscribe(data => {
              if (data.response === 'success') {
                  this.adding_message = 'This domaine was added successfully';
                  this.listOfDomaines();
              } else {
                  this.adding_message = 'Error when adding this domaine';
              }
          });
  }
  listOfDomaines() {
        this.domaineService.listDomaine().subscribe(data => this.all_domaine = data);
  }
  delete_domaine(domaineNumber: number) {
      this.domaineService.deleteDomaine(domaineNumber).subscribe(data => {
          if (data.response === 'success') {
              this.delete_message = 'this domaine was deleted succesfully';
              this.listOfDomaines();
              console.log(data);
          } else {
              console.log(data);
              this.delete_message = 'this domaine was not deleted surely cause by an error';
          }
      });
  }

    onIcontaped(event) {
        this.domaine_interact.icon = event.target.value;
    }

    onTitle(event) {
        this.domaine_interact.title = event.target.value;
    }

    onColor(event) {
        this.domaine_interact.color = event.target.value;
    }

    onDescription(event) {
        this.domaine_interact.description = event.target.value;
    }

    onTextColor(event) {
        this.domaine_interact.textcolor = event.target.value;
    }
}
