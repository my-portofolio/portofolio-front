import { Component, OnInit } from '@angular/core';
import {AdminDataAllService} from '../_services/admin-data-all.service';
import {User} from '../../shared/Models/User';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.scss']
})
export class PreferencesComponent implements OnInit {
  all_user_data: User = new User();
  form_description: FormGroup;
  description = '';
  constructor(private _request: AdminDataAllService,
              private formBuilder: FormBuilder) {
      this.form_description = this.formBuilder.group({
          'description' : [null, Validators.required]
      });
  }

  ngOnInit() {
    this.loadUserData();
  }

  loadUserData(): void {
      this._request.getUserData().subscribe(data => this.all_user_data = data);
  }

  change_Description(post) {
      this.description = post.description;
     this._request.changeDescription(this.description).subscribe(data => {
            if (data.response === 'success') {
                this.form_description.reset();
                this.loadUserData();
            } else {
                alert('error');
            }
         }
     );
  }


}
