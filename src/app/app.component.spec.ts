import { TestBed, async } from '@angular/core/testing';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { NavbarComponent } from './client/navbar/navbar.component';
import { FooterComponent } from './client/footer/footer.component';
import {MDBBootstrapModule, NavbarModule} from 'angular-bootstrap-md';
import {Styleparse} from './styleparse';
import {HttpClientModule} from '@angular/common/http';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
          MDBBootstrapModule.forRoot(),
          NavbarModule,
          HttpClientModule],
      declarations: [
        AppComponent,
          NavbarComponent,
          FooterComponent,

      ],
        providers: [
            Styleparse
        ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

});
