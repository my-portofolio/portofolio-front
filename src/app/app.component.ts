import {Component, OnInit} from '@angular/core';
import 'angular-bootstrap-md';
import {ClientDataService} from './client/client-data.service';
import {Website} from './shared/Models/Website';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  website_config = new Website(0, '', '', '', '');
  title = this.website_config.web_brand;

  constructor(private dataservice: ClientDataService) {}
    ngOnInit(): void {
      this.dataservice.get_website_data()
          .subscribe(data => this.website_config = data);
      this.title = this.website_config.web_brand;
    }
    getPage(outlet) {
        return outlet.activatedRouteData['page'] || 'main';
    }

}
