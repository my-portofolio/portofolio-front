export class Projects {
    project_id: number;
    project_title: string;
    project_description: string;
    project_categorie: string;
    project_link: string;
    project_image: File;
    project_color: string;
    project_btncolor: string;


    constructor(project_title: string, project_description: string,
                project_categorie: string, project_link: string,
                project_image: File, project_color: string, project_btncolor: string) {
        this.project_title = project_title;
        this.project_description = project_description;
        this.project_categorie = project_categorie;
        this.project_link = project_link;
        this.project_image = project_image;
        this.project_color = project_color;
        this.project_btncolor = project_btncolor;
    }
}
