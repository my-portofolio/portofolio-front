export class User {
   user_id: number;
   user_name: string;
   user_email: string;
   user_nickname: string;
   user_datebirth: string;
   user_desc: string;
   remember_token: string;
}
