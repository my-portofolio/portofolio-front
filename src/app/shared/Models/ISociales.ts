export interface ISociales {
    sociale_id: number;
    sociale_title: string;
    sociale_icon: string;
    sociale_link: string;
}
