export class Website {
    private _web_id: number;
    private _web_name: string;
    private _web_title: string;
    private _web_brand: string;
    private _web_color: string;


    constructor(web_id: number = 0, web_name: string = '', web_title: string= '', web_brand: string= '', web_color: string= '') {
        this._web_id = web_id;
        this._web_name = web_name;
        this._web_title = web_title;
        this._web_brand = web_brand;
        this._web_color = web_color;
    }

    get web_id(): number {
        return this._web_id;
    }

    set web_id(value: number) {
        this._web_id = value;
    }

    get web_name(): string {
        return this._web_name;
    }

    set web_name(value: string) {
        this._web_name = value;
    }

    get web_title(): string {
        return this._web_title;
    }

    set web_title(value: string) {
        this._web_title = value;
    }

    get web_brand(): string {
        return this._web_brand;
    }

    set web_brand(value: string) {
        this._web_brand = value;
    }

    get web_color(): string {
        return this._web_color;
    }

    set web_color(value: string) {
        this._web_color = value;
    }
}
