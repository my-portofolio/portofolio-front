export class Skills {

    skills_id: number;
    skills_color: string;
    skills_name: string;
    skills_pourc: string;

    constructor(skills_color: string, skills_name: string, skills_pourc: string) {
        this.skills_color = skills_color;
        this.skills_name = skills_name;
        this.skills_pourc = skills_pourc;
    }
}
