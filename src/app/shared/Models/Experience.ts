export class Experience {
    exp_id: number;
    exp_title: string;
    exp_date: string;
    exp_description: string;


    constructor(exp_title: string, exp_date: string, exp_description: string) {
        this.exp_title = exp_title;
        this.exp_date = exp_date;
        this.exp_description = exp_description;
    }
}
