export class Domaines {
    domaine_id: number;
    domaine_title: string;
    domaine_icon: string;
    domaine_description: string;
    domaine_color: string;
    domaine_textcolor: string;

    constructor( domaine_title: string, domaine_icon: string,
                 domaine_description: string, domaine_color: string,
                 domaine_textcolor: string) {
        this.domaine_title = domaine_title;
        this.domaine_icon = domaine_icon;
        this.domaine_description = domaine_description;
        this.domaine_color = domaine_color;
        this.domaine_textcolor = domaine_textcolor;
    }
}
