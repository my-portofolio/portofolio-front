import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from './Models/User';
import {Website} from './Models/Website';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

    private url_client = 'http://127.0.0.1:8000/';
    constructor(private _request: HttpClient) { }
    login_verify(username: string, password: string): Observable <User> {
        return this._request.get<User>(this.url_client.concat('user_login'), {params: {'email': username, 'password': password}})
            .pipe(map(user => {
                if (user && user.remember_token) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
                return user;
            }));
    }
    logout() {
        localStorage.removeItem('currentUser');
    }
}
