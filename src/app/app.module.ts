import { NgModule } from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './client/navbar/navbar.component';
import { FooterComponent } from './client/footer/footer.component';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';

import {Styleparse} from './styleparse';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {DataServiceService} from './client/_services/data-service.service';
import {PortoInterceptorService} from './client/_services/porto-interceptor.service';
import {AuthService} from './client/_services/auth.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {ClientLayoutComponent} from './client/Layout/ClientLayout';
import {LayoutParserComponent} from './admin/Layout/LayoutParser';
import {AdminNavComponent} from './admin/admin-nav/admin-nav.component';
import {AdminFooterComponent} from './admin/admin-footer/admin-footer.component';
import {LoginComponent} from './admin/login/login.component';



@NgModule({
  declarations: [
    AppComponent,
      NavbarComponent,
      FooterComponent,
      PageNotFoundComponent,
      ClientLayoutComponent,
      LayoutParserComponent,
      AdminNavComponent,
      AdminFooterComponent,
      LoginComponent
  ],
  imports: [
    AppRoutingModule,
      MDBBootstrapModule.forRoot(),
      HttpClientModule,
      BrowserAnimationsModule,
      ReactiveFormsModule,
      FormsModule
  ],
  providers: [
      Styleparse,
      DataServiceService,
      AuthService,
      {provide: HTTP_INTERCEPTORS,
      useClass: PortoInterceptorService,
      multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
