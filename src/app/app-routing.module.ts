import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {ClientLayoutComponent} from './client/Layout/ClientLayout';
import {LayoutParserComponent} from './admin/Layout/LayoutParser';
import {LoginComponent} from './admin/login/login.component';
import {AuthGuard} from './_guards/auth.guard';


const routes: Routes = [
    {path: '', redirectTo: 'client', pathMatch: 'full'},
    {path: 'login', component: LoginComponent},
    {path: 'client', component: ClientLayoutComponent, loadChildren: './client/client.module#ClientModule'},
    {path: 'admin_porto', component: LayoutParserComponent, loadChildren: './admin/admin.module#AdminModule', canActivate : [AuthGuard]},
    {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
