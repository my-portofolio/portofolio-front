# Project Title

Portofolio Frontend

## Getting Started

this is the creation of a portofolio for developer or designer totally modular and dynamic
 it is cinder in two parts the frontend part which is developed thanks to angular 7 as well as the backend part developed thanks to Laravel
### Prerequisites

before installing the front end version I advised you to download the back version here is the link
```
git@gitlab.com:my-portofolio/portofolio-back.git
```

after installation then you can install the front end
### Installing

clone the project through :
```
    git@gitlab.com:my-portofolio/portofolio-front.git
```
then got to the directorie of the and type

```
npm install
```

And then
you will need to configure the host of the backend
go to the file client and admin module for changing host

## Running

```
ng serve
```

## url for admin

```
/admin_porto
```



## Built With

* [Angular](https://angular.io/) - The web framework used


## Authors

* **axel mwenze** - *Initial work* - [axel mwenze](https://gitlab.com/alexandre2908)

## License

This project is licensed under the MIT License


